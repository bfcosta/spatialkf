#include <scene.hpp>
#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <cassert>
#include <cmath>

/**
 * draw RGB axis with basis orthogonal directions for debugging purposes.
 */
void Scene::drawAxis() {
    glColor3f(1,0,0);
    glBegin(GL_LINES);
      glVertex3f(0.0f,0.0f,0.0f);
      glVertex3f(0.5f,0.0f,0.0f);
    glEnd();
    glColor3f(0,1,0);
    glBegin(GL_LINES);
      glVertex3f(0.0f,0.0f,0.0f);
      glVertex3f(0.0f,0.5f,0.0f);
    glEnd();
    glColor3f(0,0,1);
    glBegin(GL_LINES);
      glVertex3f(0.0f,0.0f,0.0f);
      glVertex3f(0.0f,0.0f,0.5f);
    glEnd();
}

/**
 * handle display settings for glut
 */
void Scene::handleDisplay() {
    /*  
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective (angle*(zzref->scale), ratio, zn, zf);
    gluLookAt(eye[0], eye[1], eye[2], center[0], center[1], center[2], up[0], up[1], up[2]);
    */
    glMatrixMode(GL_MODELVIEW);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    //glPushMatrix();
    //glTranslatef( zzref->focus.x - zzref->center.x, zzref->focus.y - zzref->center.y, zzref->focus.z - zzref->center.z);
    //glMultMatrixf(abref->getMultMatrix());
    //glTranslatef( zzref->center.x - zzref->focus.x, zzref->center.y - zzref->focus.y, zzref->center.z - zzref->focus.z);
    anim.display();
    //drawAxis();
    //glPopMatrix();
    glutSwapBuffers();
}

/**
 * \param w new width for viewport
 * \param h new height for viewport
 * 
 * handle reshape for glut
 */
void Scene::handleReshape(int w, int h) {
    height = h;
    ratio = ((float) w)/ h;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //gluPerspective (angle*(zzref->scale), ratio, zn, zf);
    gluPerspective (angle, ratio, zn, zf);
    gluLookAt(eye[0], eye[1], eye[2], center[0], center[1], center[2], up[0], up[1], up[2]);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //abref->updateGlEnv();
}

/**
 * get scene bone animation handler
 */
Animation const & Scene::getAnimation() {
    return anim;
}

/**
 * initialization: load default settings for scene camera 
 * position, lighting and rendering
 */
void Scene::loadDefaults() {
    // camera position settings
    zn = 22.0f;
    zf = 82.0f;
    float r = (zf - zn)/2;
    up[0] = 0.0f;
    up[1] = 1.0f;
    up[2] = 0.0f;
    center[0] = 0.0f;
    center[1] = 0.0f;
    center[2] = 0.0f;
    eye[0] = center[0];
    eye[1] = center[1];
    eye[2] = center[2] + r + zn;

    // light settings
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    GLfloat pos0[4], spotdir[3];
    pos0[0] = center[0] + r*5; 
    pos0[1] = center[1] + r*5; 
    pos0[2] = center[2] + r*5; 
    pos0[3] = 1.0f;
    spotdir[0] = center[0] - pos0[0];
    spotdir[1] = center[1] - pos0[1];
    spotdir[2] = center[2] - pos0[2];
    glLightfv(GL_LIGHT0, GL_POSITION, pos0);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, spotdir);
    /*
    GLfloat amb[] =  { 0.0f, 0.0f, 0.0f, 1.0f};
    GLfloat diff[] =  { 1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat spec[] = { 1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat shininess[] = { 60.0f };
    glLightfv(GL_LIGHT0, GL_AMBIENT, amb);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diff);
    glLightfv(GL_LIGHT0, GL_SPECULAR, spec);
    glLightfv(GL_LIGHT0, GL_SHININESS, shininess);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 45.0);
    glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 2.0);
    glMaterialfv(GL_FRONT, GL_SHININESS, shininess);
    glMaterialfv(GL_FRONT, GL_SPECULAR, spec);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, diff);
    glMaterialfv(GL_FRONT, GL_AMBIENT, amb);
    */
    // render settings
    glEnable(GL_DEPTH_TEST);
    glDepthRange(0.0, 1.0);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glClearColor(0.0, 0.0, 0.0, 0.0);
}

/**
 * \param key ascii code for key pressed
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * handle keyboard events for scene
 */
void Scene::handleKeyboard(unsigned char key, int x, int y) {
    switch (key) {
        case 81: // q ou Q
        case 113:
            exit(0);
        case 27: // esc
            mode = EMPTY;
            std::cout << "Application at empty mode" << std::endl;
            break;			
        case 83: // s ou S
        case 115:
            mode = SELECTION;
            std::cout << "Application at selection mode" << std::endl;
            break;
        case 65: // a ou A
        case 97:
            mode = ANIMATION;
            anim.setContext();
            std::cout << "Application at animation mode" << std::endl;
            break;
        case 66: // b ou B
        case 98:
            mode = ARCBALL;
            std::cout << "Application at arcball mode" << std::endl;
            // todo
            break;
        case 75: // k ou K
        case 107:
            mode = FRAMEREC;
            std::cout << "Application at selection mode. Recording new frame" << std::endl;
            break;
        case 49: // 1
            anim.getSkeleton().loadHumanSkeleton();
            std::cout << "Loaded human skeleton" << std::endl;
            break;
        case 50: // 2
            anim.getSkeleton().loadBatSkeleton();
            std::cout << "Loaded bat skeleton" << std::endl;
            break;
        case 51: // 3
            anim.getSkeleton().loadOstrichSkeleton();
            std::cout << "Loaded ostrich skeleton" << std::endl;
            break;
        case 52: // 4
            anim.getSkeleton().loadFishSkeleton();
            std::cout << "Loaded fish skeleton" << std::endl;
            break;
    }
    glutPostRedisplay();
}


/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * \param button used mouse button
 * \param state mouse button statte
 * 
 * handle mouse click events for scene
 */
void Scene::handleMouseClick(int button, int state, int x, int y) {
	float sx, sy;
	
	sx = x / (ratio * height);
	sy = y / height;
	switch (mode) {
		case SELECTION:
			if (button == GLUT_LEFT_BUTTON) {
				if (state == GLUT_DOWN) {
					if (selectBoneJoint(x, y)) startBoneJointTransformation(x, y);
					else selectMarkerController(x, y);
				}
			}
			break;
		case ANIMATION:
			if (button == GLUT_LEFT_BUTTON) {
				if (state == GLUT_DOWN) {
					selectMarkerController(x, y);
				} else mode = EMPTY;
			}
			break;
		case FRAMEREC:
			if (button == GLUT_LEFT_BUTTON) {
				if (state == GLUT_DOWN) {
					anim.addNewFrame(sx,sy);
				} else {
					mode = SELECTION;
					glutPostRedisplay();
				}
			}
			break;
		case ARCBALL:
			break;
		default:
			break;
	}
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * handle mouse motion events for scene
 */
void Scene::handleMouseMotion(int x, int y) {

	switch (mode) {
		case SELECTION:
			if (clickHit) updateBoneJoint(x, y);
			else {
				trackController(x,y);
				trackMarker(x,y);
			}
			break;
		case FRAMEREC:
			trackMarker(x,y);
			trackController(x,y);
			break;
		case ANIMATION:
			trackController(x, y);
			anim.animate();
			break;
		case ARCBALL:
			break;
		default:
			break;
	}
	glutPostRedisplay();
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * \param selectBuf pointer to selection buffer
 * \param size selection buffer size
 * 
 * fill selection buffer with hit objects in the scene
 */
unsigned int Scene::getSelectHits(int x, int y, unsigned int * selectBuf ,unsigned int size) {
	GLint vp[4];
	double pix = 5.0;
	
    glGetIntegerv(GL_VIEWPORT, vp);
    glSelectBuffer(size, selectBuf);
    glRenderMode(GL_SELECT); // selection mode on
    glInitNames();
	glPushName(100);	
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluPickMatrix((GLdouble) x, (GLdouble) vp[3] - y, pix, pix, vp);
    gluPerspective (angle, ratio, zn, zf);
    gluLookAt(eye[0], eye[1], eye[2],
                center[0], center[1], center[2],
                up[0], up[1], up[2]);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    //glMultMatrixf(abref->getMultMatrix());
    //glTranslatef( zzref->focus.x - zzref->center.x, 
		//zzref->focus.y - zzref->center.y, zzref->focus.z - zzref->center.z);
    //glScalef(zzref->scale, zzref->scale, zzref->scale);
    //glTranslatef( zzref->center.x - zzref->focus.x, 
		//zzref->center.y - zzref->focus.y, zzref->center.z - zzref->focus.z);
    
    anim.getSkeleton().draw(GL_SELECT);
    anim.drawMarkers(GL_SELECT);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glutSwapBuffers();
    return glRenderMode(GL_RENDER);
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 *  get selected marker and/or controller if it exists
 */ 
bool Scene::selectMarkerController(int x, int y) { 
	GLuint selectBuf[BUFSIZE];
	unsigned int name, hits, ccan, mcan;
	float minz, upl, downl;
	std::vector<int> cn, mn;
	std::vector<float> cnz, mnz;
	bool hit;
		
    hits = getSelectHits(x, y, selectBuf, BUFSIZE);
	anim.getSkeleton().sJoint = anim.getSkeleton().joints.size();
	anim.getSkeleton().sBone = anim.getSkeleton().bones.size();
	anim.sMarker = anim.getMarkers().size();
	anim.sContr = anim.controller.size();
	downl = anim.getSkeleton().bones.size() + anim.getSkeleton().joints.size();
	upl = downl + anim.getMarkers().size();
	for (unsigned int i = 0, k = 0; i < hits; ++i, k = selectBuf[k] + k + 3) {
        for (unsigned int j = 0; j < selectBuf[k]; ++j) {
            name = selectBuf[k + j + 3];
            minz = selectBuf[k + j + 4]/0x7fffffff;
			if (name >= downl) { 
				if (name < upl) {
					mn.push_back(name - downl);
					mnz.push_back(minz);					
				} else {
					cn.push_back(name - upl);
					cnz.push_back(minz);										
				}
			}
        }
    }
    ccan = cn.size();
    mcan = mn.size();
    for (unsigned int i=0; i < cnz.size(); ++i) 
		if (minz >= cnz.at(i)) {
			minz = cnz.at(i);
			ccan = i;
		}
    for (unsigned int i=0; i < mnz.size(); ++i) 
		if (minz >= mnz.at(i)) {
			minz = mnz.at(i);
			mcan = i;
		}
	hit = !((mcan == mn.size())&&(ccan == cn.size()));
	if (hit) {
		if (mcan < mn.size()) anim.sMarker = mn.at(mcan);
		if (ccan < cn.size()) anim.sContr = cn.at(ccan);
		glutPostRedisplay();
	}
	//std::cout << "selected = " << hit << std::endl;
	return hit;
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * update selected marker position.
 */
void Scene::trackMarker(int x, int y) {
	float sx, sy;

	sx = x / (ratio * height);
	sy = y / height;
	if (anim.sMarker != anim.getMarkers().size()) {
		anim.getMarkers().at(anim.sMarker)(0) = sx;
		anim.getMarkers().at(anim.sMarker)(1) = sy;
	}
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * update selected controller position.
 */
void Scene::trackController(int x, int y) {
	float sx, sy;

	sx = x / (ratio * height);
	sy = y / height;
	if (anim.sContr != anim.controller.size()) {
		anim.controller.at(anim.sContr).scoord(0) = sx;
		anim.controller.at(anim.sContr).scoord(1) = sy;
	}	
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 *  select bone for rotation or joint for translation.
 */
 bool Scene::selectBoneJoint(int x, int y) {
	GLuint selectBuf[BUFSIZE];
	unsigned int name, hits, jcan, bcan;
	float minz, downl, upl;
	std::vector<int> bn, jn;
	std::vector<float> bnz, jnz;
	
    hits = getSelectHits(x, y, selectBuf, BUFSIZE);
	// second part: process hits
	bn.clear();
	bnz.clear();
	jn.clear();
	jnz.clear();
	anim.getSkeleton().sJoint = anim.getSkeleton().joints.size();
	anim.getSkeleton().sBone = anim.getSkeleton().bones.size();
	anim.sMarker = anim.getMarkers().size();
	anim.sContr = anim.controller.size();
	upl = anim.getSkeleton().joints.size() + anim.getSkeleton().bones.size();
	downl = anim.getSkeleton().joints.size();
	// get bones or joint point candidates
	for (unsigned int i = 0, k = 0; i < hits; ++i, k = selectBuf[k] + k + 3) {
        for (unsigned int j = 0; j < selectBuf[k]; ++j) {
            name = selectBuf[k + j + 3];
            minz = selectBuf[k + j + 4]/0x7fffffff;
            if (name < upl) {
				if (name >= downl) { // bone hit
					bn.push_back(name - downl);
					bnz.push_back(minz);
				} else {	// joint hit
					jn.push_back(name);
					jnz.push_back(minz);					
				}
			}
        }
    }
    // get upper hit if exists
    jcan = jn.size();
    bcan = bn.size();
    //std::cout << "jcan = " << jcan << " bcan = " << bcan << std::endl;
    for (unsigned int i=0; i<jnz.size(); ++i) 
		if (minz >= jnz.at(i)) {
			minz = jnz.at(i);
			jcan = i;
		}
    for (unsigned int i=0; i<bnz.size(); ++i) 
		if (minz >= bnz.at(i)) {
			minz = bnz.at(i);
			bcan = i;
		}
	// has hit ?
	clickHit = !((jcan == jn.size())&&(bcan == bn.size()));
	if (clickHit) {
		// hit joint
		if (jcan < jn.size()) {
			if ((bcan < bn.size())&&(bnz.at(bcan) < jnz.at(jcan))) {
				// hit also upper bone
				anim.getSkeleton().sJoint = 
					anim.getSkeleton().bones.at( bn.at(bcan) ).current();
				// upper joint or no bone hit 
			} else anim.getSkeleton().sJoint = jn.at(jcan);
		// hit only bone
		} else anim.getSkeleton().sJoint = 
			anim.getSkeleton().bones.at( bn.at(bcan) ).current();
		glutPostRedisplay();
	}
	//std::cout << "selected = " << clickHit << std::endl;
	return clickHit;
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * fix points for bone rotation or joint translation.
 */
void Scene::startBoneJointTransformation(int x, int y) {
	unsigned int parent, current;
    double mv[16], pm[16];
    int vp[4];
	double ox, oy, oz, nx, ny, nz;
	float *pref;
	
	assert(clickHit);
	current = anim.getSkeleton().sJoint;
	parent = anim.getSkeleton().joints.size();
	anim.getSkeleton().sBone = anim.getSkeleton().bones.size();
	for (unsigned int i=0; i<anim.getSkeleton().bones.size(); ++i) {
		if (anim.getSkeleton().bones.at(i).current() == current) {
			parent = anim.getSkeleton().bones.at(i).parent();
			anim.getSkeleton().sBone = i;
			bonePrev = anim.getSkeleton().bones.at(i).getMatrix();
		}
	}
	glGetDoublev(GL_MODELVIEW_MATRIX, mv);
	glGetDoublev(GL_PROJECTION_MATRIX, pm);
	glGetIntegerv(GL_VIEWPORT, vp);
	//std::cout << "parent = " << parent << " current = " << current 
	//	<< " joints = " << anim.getSkeleton().joints.size()<< std::endl;
	if (parent != anim.getSkeleton().joints.size()) {
		pref = anim.getSkeleton().joints.at(parent).jpos.data();
		// get rotation center
		gluProject(pref[0], pref[1], pref[2], mv, pm, vp, &ox, &oy, &oz);
		gluUnProject(ox , oy, 0, mv, pm, vp, &nx, &ny, &nz);
		rcen(0) = (float) nx;
		rcen(1) = (float) ny;
		rcen(2) = (float) nz;
		rcen(3) = 1;
		// get begin
		gluUnProject(x , vp[3] - y, 0, mv, pm, vp, &nx, &ny, &nz);
		rini(0) = (float) nx;
		rini(1) = (float) ny;
		rini(2) = (float) nz;
		rini(3) = 1;
	} else {
		// get translation origin for joint
		if (current < anim.getSkeleton().joints.size()) {
			bonePrev = anim.getSkeleton().joints.at(current).getMatrix();
			rini = anim.getSkeleton().joints.at(current).jpos;
		}
	}
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * calculate and update bone rotation or joint translation.
 */
 void Scene::updateBoneJoint(int x, int y) {
    double mv[16], pm[16];
    double nx, ny, nz, zdepth;
    int vp[4];
    unsigned int jparent;
	Eigen::Vector4f rmouse, vi, vf, axis, transl;
	float *rot, *sbone, angle, sx, sy;
	Eigen::Matrix4f rotMat;

    glGetDoublev(GL_MODELVIEW_MATRIX, mv);
    glGetDoublev(GL_PROJECTION_MATRIX, pm);
    glGetIntegerv(GL_VIEWPORT, vp);
    rot = rotMat.data();
    if (anim.getSkeleton().sBone != anim.getSkeleton().bones.size())
		// rotation: znear projected plane
		zdepth = 0;
	else {
		// translation: joint projected plane
		if (anim.getSkeleton().sJoint  < anim.getSkeleton().joints.size()) {
			sbone = anim.getSkeleton().joints.at( anim.getSkeleton().sJoint ).jpos.data();
			gluProject(sbone[0], sbone[1], sbone[2], mv, pm, vp, &nx, &ny, &zdepth);
		}
	}
	gluUnProject(x , vp[3] - y, zdepth, mv, pm, vp, &nx, &ny, &nz);
	rmouse(0) = (float) nx;
	rmouse(1) = (float) ny;
	rmouse(2) = (float) nz;
	rmouse(3) = 1;
	sbone = bonePrev.data();
	// rotate selected bone
	//std::cout << "sBone = " << anim.getSkeleton().sBone;
	//std::cout << " boneSize = " << anim.getSkeleton().bones.size();
	//std::cout << " sFrame = " << anim.getFrameSet().sFrame;
	//std::cout << " frameSize = " << anim.getFrameSet().points.size();
	//std::cout << " sJoint = " << anim.getSkeleton().sJoint;
	//std::cout << " jointSize = " << anim.getSkeleton().joints.size() << std::endl;
	if (anim.getSkeleton().sBone != anim.getSkeleton().bones.size()) {
		vi = rini - rcen;
		vf = rmouse - rcen;
		axis = vi.cross3(vf);
		axis.normalize();
		angle = acos( vi.dot(vf) / (vi.norm() * vf.norm()) )*180/M_PI;
		jparent = anim.getSkeleton().bones.at(anim.getSkeleton().sBone).parent();
		transl = anim.getSkeleton().joints.at(jparent).jpos;
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glLoadMatrixf(sbone);
		glRotatef(angle, axis(0), axis(1), axis(2));
		glGetFloatv(GL_MODELVIEW_MATRIX, rot);
		glPopMatrix();
		anim.getSkeleton().bones.at(anim.getSkeleton().sBone).setMatrix(rotMat);
		anim.getSkeleton().updateBonesRef();
	} else {
		// translate selected joint or frame
		if (anim.getSkeleton().sJoint  < anim.getSkeleton().joints.size()) {
			transl = rmouse - rini;
			rmouse = rini;
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glLoadMatrixf(sbone);
			glTranslatef(transl(0), transl(1), transl(2));
			glGetFloatv(GL_MODELVIEW_MATRIX, rot);
			glPopMatrix();
			anim.getSkeleton().joints.at(anim.getSkeleton().sJoint).setMatrix(rotMat);
			anim.getSkeleton().updateBonesRef();
		} else {
			sx = x / (ratio * height);
			sy = y / height;
			if (anim.sFrame == anim.getMarkers().size()) {
				anim.controller.at(anim.contIdx).scoord(0) = sx;
				anim.controller.at(anim.contIdx).scoord(1) = sy;
				
			} else {
				anim.getMarkers().at(anim.sFrame)(0) = sx;
				anim.getMarkers().at(anim.sFrame)(1) = sy;
			}
		}
	}
}
