#ifndef SCENE_H
#define SCENE_H

#include <animation.hpp>

#define EMPTY 0
#define ARCBALL 1
#define SELECTION 2 
#define FRAMEREC 3
#define ANIMATION 4
#define BUFSIZE 4096

/**
 * \class Scene
 *
 * \brief A handler for the scene and opengl manipulations
 *
 * A class for interfacing with glut and handling UI tasks as
 * bone/joint selection, pose recording, controller tracking and so on.
 * Provides useful data for the animation handler. Read readme.txt file
 * for details on UI interface.
 *
 * \note Markers and Controller are placed on first z-buffer plane (z=0).
 * No zoom or arcball implemented yet. 
 */

class Scene {
    protected:
    /// opengl camera position
    float eye[3]; 
    /// opengl world center coordinate
    float center[3];
    /// opengl up orientation
    float up[3];
    /// opengl projection z-near
    float zn; 
    /// opengl projection z-far
    float zf;
    /// window width-height ratio
    float ratio;
    /// window height
    float height;
    /// field of view angle
    static const float angle = 45.0;
    /// current mode of application
    unsigned int mode;
    /// flag for a successful previous selection
	bool clickHit;
	/// initial position for selected bone/joint
    Eigen::Vector4f rini;
    /// center position of a selected bone's rotation
    Eigen::Vector4f rcen;
    /// old rotation matrix value for the current moving bone
    Eigen::Matrix4f bonePrev;
    /// animation handler
    Animation anim;
    
    void drawAxis();
    unsigned int getSelectHits(int x, int y, unsigned int * selectBuf ,unsigned int size);
	bool selectBoneJoint(int x, int y);
	bool selectMarkerController(int x, int y);
	void startBoneJointTransformation(int x, int y);
	void trackMarker(int x, int y);
	void trackController(int x, int y);
	void updateBoneJoint(int x, int y);

    public:
    
    /**
     * Scene initializer
     */
    Scene() { clickHit = false; mode = EMPTY; }
    void loadDefaults();
    void handleDisplay();
    void handleReshape(int w, int h);
    void handleMouseClick(int button, int state, int x, int y);
    void handleMouseMotion(int x, int y);
    void handleKeyboard(unsigned char key, int x, int y);
    Animation const & getAnimation();
};

#endif
