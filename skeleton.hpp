#ifndef SKELETON_H
#define SKELETON_H

#include <bone.hpp>
#include <drawer.hpp>
#include <GL/gl.h>

/**
 * \class Skeleton
 *
 * \brief Models the skeleton
 *
 * A class representing the skeleton. Here, a skeleton is a set of
 * N bones and N+1 joints. The first joint position is a shift on
 * global coordinates and other joint positions are relative shifts to
 * its parent joint considering the rotation hierarchy existant for
 * each bone.
 * 
 */

class Skeleton : public ShapeDrawer {
    public:
    /// vector of skeleton joints
    std::vector<Junction> joints;
    /// vector of skeleton bones
    std::vector<Bone> bones;
    /// selected joint id
	unsigned int sJoint;
	/// selected bone id
	unsigned int sBone;

	Eigen::Matrix4f rotateAtCenter(Eigen::Matrix4f const & rotmat, 
		Eigen::Vector4f const & center);
	
    Skeleton() : ShapeDrawer() {}
	virtual ~Skeleton();
    void addBone(float x, float y, float z, int i);
    void draw(GLenum mode);
    void updateBonesRef();
    void loadHumanSkeleton();
    void loadBatSkeleton();
    void loadOstrichSkeleton();
    void loadFishSkeleton();
    void print();
};

#endif
