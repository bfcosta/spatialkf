How to use:

All user interface is handled by keyboard or mouse events. No glut menu
is provided. Keyboard events are case insensitive. Application run in 
one of 4 possible modes: initial empty mode, selection mode, frame 
recording mode and animation mode. Arcball mode is not yet implemented. 

Selection mode refers to building poses for the skeleton. Once it is 
loaded, the user can select and move joints or bones to build a new pose.
To enter on selection mode, press S/s key. Once the pose is built, the user
might want to save it for future interpolations. Frame recording mode 
saves the current pose. To enter on frame recording mode, press K key.
After pressing K/k, do a left mouse button click release to associate a 
marker with the recorded pose. The pose will only be recorded if there a
new marker is inserted. If the user wants to replace the marker,
get back to selection mode, then select the marker and move it. After 
two or more poses are recorded, the user can animate the skeleton moving
the controller between markers positions. This is done on Animation mode.
To get into Animation mode, press A/a. The user can also enter into an
empty mode, pressing ESC. The empty mode exists to avoid that any mouse
evento to make any changes. The application starts at empty mode.

The application starts without a loaded skeleton. The user can load one 
of four possible skeletons at any application mode, by pressing keys 1 
to 4. Pressing Q/q will make the application exit. Here is a summary of
all key bindings of this UI:

	* Esc: enters empty mode
	* S/s: enters selection mode
	* K/k: enters frame recording mode
	* A/a: enters animation mode
	* Q/q: quit application
	* 1: loads a human skeleton
	* 2: loads a bat skeleton
	* 3: loads a ostrich skeleton
	* 4: loads a fish skeleton

Any doubts, please mail to: bfcosta@cos.ufrj.br
