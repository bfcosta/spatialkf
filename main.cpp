#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <scene.hpp>

Scene *s;

/**
 * glut display function
 */

void display() {
    s->handleDisplay();
}

/**
 * glut reshape funtion
 */

void reshape(int w, int h) {
    s->handleReshape(w, h);
}

/**
 * glut keyboard function
 */

void keyboard(unsigned char key, int x, int y){
    s->handleKeyboard(key, x, y);
}

/**
 * glut mouse click
 */

void mouseClick(int button, int state, int x, int y) {
   s->handleMouseClick(button, state, x, y);
}

/**
 *    glut mouse move
 */

void mouseMotion(int x, int y) {
    s->handleMouseMotion(x, y);
}

int main (int argc, char ** argv) {
    
    s = new Scene();
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowPosition(100, 100);
    glutInitWindowSize(800, 600);
    glutCreateWindow("Squirrel");
    s->loadDefaults();
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouseClick);
    glutMotionFunc(mouseMotion);
    glutMainLoop();
    delete s;
    return 0;
}
