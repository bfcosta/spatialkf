#ifndef BONE_H
#define BONE_H

#include <Eigen/Dense>
#include <vector>

/**
 * \class Junction
 *
 * \brief Models a skeleton junction or an end-effector
 *
 * A class representing the extremities of a bone. Contains only
 * a translational matrix and a volatile accumulator for calculating
 * its position on global coordinates.   
 * 
 * \note This class has no rotation attribute which should belong 
 * to the bone itself.
 *
 */

class Junction {
	protected:
	/// a translation matrix defining its shift
	Eigen::Matrix4f trans;
	
	public:
	/// a temporary variable for the position of the junction
	Eigen::Vector4f jpos;

	/// creates a new junction with a null shift
	Junction() {
		trans = Eigen::Matrix4f::Identity();
	}
	
	/**
	 * \param x,y,z shift vector
	 * 
	 * creates a new junction with (x,y,z) shift related to
	 * position of parent joint.
	 */
	Junction(float x, float y, float z) {
		trans = Eigen::Matrix4f::Identity();
		set(x, y, z);
	}
	
	/**
	 * \param x,y,z shift vector
	 * 
	 * defines a junction shift (x,y,z) related to
	 * position of parent joint.
	 */
	void set(float x, float y, float z) {
		trans(0, 3) = x;
		trans(1, 3) = y;
		trans(2, 3) = z;
	}
	
	/**
	 * \return translation matrix with joint shift
	 * 
	 * returns joint shift
	 */
	Eigen::Matrix4f const & getMatrix() { 
		return trans;
	}
	
	/**
	 * \param m translation shift matrix
	 * 
	 * defines the joint translation matrix
	 */
	void setMatrix(Eigen::Matrix4f const & m) {
		trans = m;
	}
};

/**
 * \class Bone
 *
 * \brief Models a skeleton bone.
 *
 * A class representing the bone itself. A bone is defined by
 * references for two distinct joints and a rotation matrix.
 * The two joints are the two extremities of the bone and the
 * rotation matrix defines a rotation for the end joint relative
 * to initial joint.
 *
 * \note 
 *
 */

class Bone {
    protected:
    /// reference for parent (initial) joint in vector
    unsigned int par;
    /// reference for current (end) joint in vector
    unsigned int curr;
    /// rotation matrix for the bone
	Eigen::Matrix4f rot;

    public:
    /**
     * \param ii reference for the bone's joint begin
     * \param ff reference for the bone's joint end
     * 
     * creates a new bone with tow given joints and no rotation.
     */
    Bone(unsigned int ii, unsigned int ff) {
        par = ii;
        curr = ff;
        rot = Eigen::Matrix4f::Identity();
    }
    
    /**
     * \return parent joint position on skeleton joint vector
     * 
     * returns parent joint reference
     */
    unsigned int parent() {
        return par;
    }
    
    /**
     * \return current joint position on skeleton joint vector
     * 
     * returns current joint reference
     */
    unsigned int current() {
		return curr;
	}
	
	/**
     * \return current rotation matrix for this bone
     * 
     * returns bone's rotation matrix
	 */
	Eigen::Matrix4f const & getMatrix() {
		return rot;
	}
	
	/**
     * \param m a rotation matrix 
     * 
     * defines a rotation matrix for this bone
	 */
	void setMatrix(Eigen::Matrix4f const & m) {
		rot = m;
	}
};

#endif
