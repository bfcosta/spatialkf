#ifndef ANIMATION_H
#define ANIMATION_H

#include <skeleton.hpp>
#include <rbf.hpp>

/**
 * \class Controller
 *
 * \brief models the controller of the animation
 *
 * A class for the animation controller. The controller is the main
 * animation handler to get interpolated poses. It consists of a position
 * on marker space. It is rendered as a magenta ball. If the position is 
 * equal to any marker (cyan balls), the generated pose should be 
 * equal to the one recorded pose at that marker. Otherwise, a RBF 
 * interpolation is made.
 * 
 * \see Animation
 */

class Controller {
	public:
	/// coordinate position of the controller
	Eigen::Vector4f scoord;
	/// array of indices to markers relative to this controller
	/// currently not in use
	std::vector<unsigned int> mref;
	
	/**
	 * creates a new controller
	 */
	Controller() {}
};

/**
 * \class Animation
 *
 * \brief A handler for the animation process
 *
 * This class encapsulates the basic animation process. The animation 
 * process in short is: record a skeleton frame, associate it with a 
 * marker position and later move the controller between the markers to
 * get an interpolated pose. The animation builds the interpolated 
 * skeleton.
 *
 * \note current implementation is with a single controller.
 */

class Animation : public ShapeDrawer{
    protected:
    /// current interpolated skeleton
    Skeleton skel;
    /// array of markers
   	std::vector<Eigen::Vector4f> markers;
   	/// array of recorded skeletons
	std::vector<Skeleton> frameset;
	/// RBF interpolator handler
	RbfSkelInterp rbfint;
	/// projection matrix
	double pm[16];

	/**
	 * function for debugging purposes
	 */ 
	void printMarkers();

    public:
    /// controller of the animation
    std::vector<Controller> controller;
    /// selected frame id
    unsigned int sFrame;
    /// selected marker id
    unsigned int sMarker;
    /// selected controller id
    unsigned int sContr;
    /// index to current controller on controller vector
    unsigned int contIdx;
    
    /**
     * creates a new animation
     */
    Animation() : ShapeDrawer() {
		contIdx = 0;
		controller.push_back(Controller());
	}
	
	/**
	 * clears all animation data
	 */
    virtual ~Animation() {
		markers.clear();
		frameset.clear();
		controller.clear();
	}

    void display();
    void setContext();
    Skeleton & getSkeleton();
    std::vector<Skeleton> const & getFrameSet();
    std::vector<Eigen::Vector4f> & getMarkers();
    void addNewFrame(float x, float y);
    void addNewController(float x, float y);
	void animate();
    void drawMarkers(GLenum mode);
};

#endif
