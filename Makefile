CXX=g++
CXXFLAGS=-I. -I/usr/include/eigen3 -Wall
LDFLAGS=-lGL -lGLU -lglut
SOURCES=$(wildcard *.cpp)
DEPS=$(wildcard *.hpp)
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=main

all:    $(EXECUTABLE)

$(EXECUTABLE):  $(OBJECTS) 
	$(CXX) -o $@ $(OBJECTS) $(LDFLAGS)

%.o: %.cpp $(DEPS)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	rm -f *~ *.o $(EXECUTABLE)

