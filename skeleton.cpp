#include <skeleton.hpp>
#include <iostream>
#include <cmath> 

/**
 *  default destructor
 */

Skeleton::~Skeleton() {
	bones.clear();
	joints.clear();
}

/**
 * \param rotmat a rotation matrix centered at (0,0,0)
 * \param center a translation matrix designing a center (cx,cy,cz) for the rotation
 * \return a rotation matrix centered at (cx,cy,cz)
 * 
 * returns rotation matrix centered at center point.
 */
Eigen::Matrix4f Skeleton::rotateAtCenter(Eigen::Matrix4f const & rotmat, 
	Eigen::Vector4f const & center) {
		
	Eigen::Matrix4f t1, t2;
	t1 = Eigen::Matrix4f::Identity();
	t2 = Eigen::Matrix4f::Identity();
	t1(0,3) = center(0);
	t1(1,3) = center(1);
	t1(2,3) = center(2);
	t2(0,3) = (-1) * center(0);
	t2(1,3) = (-1) * center(1);
	t2(2,3) = (-1) * center(2);
	return (t1 * rotmat * t2);
}

/**
 * \param x X component
 * \param y Y component
 * \param z Z component
 * \param i parent joint reference
 * 
 *  add a new bone to skeleton where (x,y,z) is the final joint and i
 *  is the parent joint
 */
void Skeleton::addBone(float x, float y, float z, int i) {
    if (i!=0) bones.push_back(Bone(i-1, joints.size()));
    joints.push_back(Junction(x, y, z));
}

/**
 *  find at once where each joint is at world coordinates
 */
void Skeleton::updateBonesRef() {
	Eigen::Matrix4f r,t;
	Eigen::Vector4f v;
	unsigned int cur, par;
	std::vector<Eigen::Matrix4f> rot;
	
	v << 0, 0, 0, 1;
	t = joints.at(0).getMatrix();
	joints.at(0).jpos = t * v;
	for (unsigned int i=0; i < bones.size(); ++i) {
		par = bones.at(i).parent();
		cur = bones.at(i).current();
		v = joints.at(par).jpos;
		t = joints.at(cur).getMatrix();
		if (par == 0) r = bones.at(i).getMatrix();
		else r = rot.at(par - 1) * bones.at(i).getMatrix();
		rot.push_back(r);
		joints.at(cur).jpos = rotateAtCenter(r, v) * t * v;
	}
}

/**
 *  draw skeleton joints and bones.
 */
void Skeleton::draw(GLenum mode) {
	Eigen::Vector3f v1, v2, axis;
	float angle, norm;
	
    v2 << 0,0,1;
    // Junction
    for (unsigned int i=0; i < joints.size(); ++i) {
        v1 = joints.at(i).jpos.head(3);
        if (i == sJoint) {
			glColor3f(0,1,1);
			//std::cout << "selected = (" << v1(0) << ", " << v1(1) 
			//	<< ", " << v1(2) << ")" << std::endl;
		} else glColor3f(1,1,1);
        glPushMatrix();
        glTranslatef(v1(0), v1(1), v1(2));
        if (mode == GL_SELECT) glLoadName(i);
        drawSphere(0.4, 30, 30);
        glPopMatrix();
    }   
    // Bones
    for (unsigned int i=0; i < bones.size(); ++i) {
        if (bones.at(i).current() == sJoint) glColor3f(0,1,1);
        else glColor3f(1,1,1);
        glPushMatrix();
        v1 = joints.at(bones.at(i).current()).jpos.head(3) - 
			joints.at(bones.at(i).parent()).jpos.head(3);
        norm = v1.norm();
        v1.normalize();
        axis = v2.cross(v1);
		angle = acos( v2.dot(v1) )*180/M_PI;
		v1 = joints.at(bones.at(i).parent()).jpos.head(3);
		glTranslatef(v1(0), v1(1), v1(2));
		glRotatef(angle, axis(0), axis(1), axis(2));
		if (mode == GL_SELECT) glLoadName(i + joints.size());
		drawCylinder(0.2, norm, 10, 4);
        glPopMatrix();
    }
    //print();
}

/**
 * bone adding sequence for a human skeleton form
 */
void Skeleton::loadHumanSkeleton() {
    joints.clear();
    bones.clear();
    addBone(0, 0, 0, 0);      // 0 PELVIS 1
    addBone(0, 6, 0, 1);           // 1 CHEST 2
    addBone(0, 3, 0, 2);           // 2 HEAD 3
    addBone(-4, -1.2, 0, 2);       // 2 LELBOW 4
    addBone(-3, -0, 1, 4);         // 4 LWRIST 5
    addBone(4, -1.2, 0, 2);        // 2 RELBOW 6
    addBone(3, 0, 1, 6);           // 6 RWRIST 7
    addBone(1.5, -0.5, 0, 1);      // 1 RPELVIS 8
    addBone(0, -4, 0, 8);          // 8 RKNEE 9
    addBone(0, -4, 0, 9);          // 9 RHEEL 10
    addBone(0.2, -0.2, 2, 10);      // 10 RFOOT 11
    addBone(-1.5, -0.5, 0, 1);     // 1 LPELVIS 12
    addBone(0, -4, 0, 12);          // 12 LKNEE 13
    addBone(0, -4, 0, 13);          // 13 LHEEL 14
    addBone(-0.2, -0.2, 2, 14);     // 14 LFOOT 15
	updateBonesRef();
	sJoint = joints.size();
	sBone = bones.size();
}

/**
 * bone adding sequence for a bat skeleton form
 */
void Skeleton::loadBatSkeleton() {
    joints.clear();
    bones.clear();
    addBone(0, 0, 0, 0);	// PELVIS 1
    addBone(1.5, -0.5, 0, 1);	// RPELVIS 2
    addBone(0, -2, 0.5, 2);		// RKNEE 3
    addBone(0, -2, -0.5, 3);	// RHEEL 4
    addBone(0, -0.2, 2, 4);		// RFOOT 5
    addBone(-1.5, -0.5, 0, 1);	// LPELVIS 6
    addBone(0, -2, 0.5, 6);		// LKNEE 7
    addBone(0, -2, -0.5, 7);	// LHEEL 8
    addBone(-0.2, -0.2, 2, 8);	// LFOOT 9
    addBone(0, 4, 0, 1);		// CHEST 10
    addBone(3, -2, 0, 10);		// RWING1 11
    addBone(3, 3, 0, 11);		// RWING2 12
    addBone(6, -4, 0, 12);		// RWING4 13
    addBone(2, -4.5, 0, 12);	// RWING4 14
    addBone(-1, -4, 0, 12);		// RWING5 15
    addBone(-3, -2, 0, 10);		// LWING1 16
    addBone(-3, 3, 0, 16);		// LWING2 17
    addBone(-6, -4, 0, 17);		// LWING4 18
    addBone(-2, -4.5, 0, 17);	// LWING4 19
    addBone(1, -4, 0, 17);		// LWING4 20
    addBone(0, 2, 0, 10);		// HEAD 10
	updateBonesRef();
	sJoint = joints.size();
	sBone = bones.size();
}

/**
 * bone adding sequence for a ostrich skeleton form
 */
void Skeleton::loadOstrichSkeleton() {
    joints.clear();
    bones.clear();
    addBone(0, 0, 0, 0);	// PELVIS 1
    addBone(1.5, -0.5, 0, 1);	// RPELVIS 2
    addBone(0, -4, -2, 2);		// RKNEE 3
    addBone(0, -4, 2, 3);		// RHEEL 4
    addBone(0, -0.2, 2, 4);		// RFOOT 5
    addBone(-1.5, -0.5, 0, 1);	// LPELVIS 6
    addBone(0, -4, -2, 6);		// LKNEE 7
    addBone(0, -4, 2, 7);		// LHEEL 8
    addBone(-0.2, -0.2, 2, 8);	// LFOOT 9
    addBone(0, -0.5, -2, 1);	// COCKIS 10
    addBone(0, 0.2, 3, 1);		// CHEST 11
    addBone(0, 2, 1, 11);		// NECK1 12
    addBone(0, 4, 0, 12);		// NECK2 13
    addBone(0, 1, 0.5, 13);		// HEAD 14
    addBone(0, -0.5, 1.5, 14);	// BEAK 15
    addBone(1, -0.5, -1.5, 11);	// RWING1 16
    addBone(2, -0.2, 0, 16);	// RWING2 17
    addBone(1, -0.2, -1.5, 17);	// RWING2 18
    addBone(-1, -0.5, -1.5, 11);// RWING1 19
    addBone(-2, -0.2, 0, 19);	// RWING2 20
    addBone(-1, -0.2, -1.5, 20);// RWING2 21
	updateBonesRef();
	sJoint = joints.size();
	sBone = bones.size();
}

/**
 * bone adding sequence for a fish skeleton form
 */
void Skeleton::loadFishSkeleton() {
    joints.clear();
    bones.clear();
    addBone(0, 0, 0, 0);	// COLUN 1
    addBone(-8, 0, 0, 1);		// COLUNBACK 2
    addBone(6, 0, 0, 1);		// COLUNFRONT 3
    addBone(-2, 3, 0, 2);		// TAILUP1 4
    addBone(-3, 2, 0, 4);		// TAILUP1 5
    addBone(-1.5, -2, 0, 2);	// TAILDOWN1 6
    addBone(-2, -2, 0, 6);		// TAILDOWN2 7
    addBone(-1.5, -2, 1, 3);	// LFIN1 8
    addBone(-2, -2, 0.5, 8);	// LFIN2 9
    addBone(-1.5, -2, -1, 3);	// RFIN1 10
    addBone(-2, -2, -0.5, 10);	// RFIN2 11
    addBone(3, 0, 0, 3);		// HEAD 12
	updateBonesRef();
	sJoint = joints.size();
	sBone = bones.size();
}

/**
 * print fuction for debugging purposes
 */
void Skeleton::print() {
	
	for (unsigned int i=0; i< bones.size(); ++i) {
		std::cout << "Bone " << i << std::endl;
		std::cout << "Translation" << std::endl;
		std::cout << joints.at(bones.at(i).parent()).getMatrix() << std::endl;
		std::cout << "A = (" << joints.at(bones.at(i).parent()).jpos.transpose() << ")" << std::endl;
		std::cout << "Translation" << std::endl;
		std::cout << joints.at(bones.at(i).current()).getMatrix() << std::endl;
		std::cout << "B = (" << joints.at(bones.at(i).current()).jpos.transpose() << ")" << std::endl;
		std::cout << "Rotation" << std::endl;
		std::cout << bones.at(i).getMatrix() << std::endl;
		std::cout << std::endl;
	}
}
