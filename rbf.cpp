#include <rbf.hpp>
#include <Eigen/LU>
#include <iostream>
#include <cmath>
#include <cassert>

#define PZR  1.0e-6

/**
 * \param v1 dataset point
 * \param v2 another dataset point
 * \return rbf distance between v1 and v2
 * 
 *  distance function for rbf estimation. v1 and v2 are dataset points
 */    
float RbfBaseSolver::phi(Eigen::Vector4f const & v1, Eigen::Vector4f const & v2) {
	Eigen::Vector3f diff = v2.head(3) - v1.head(3);
	return diff.norm();
}

/**
 * add imaginary marker to pretend a simple interpolation function 
 * when there are few input points
 */
void RbfBaseSolver::addRedundant() {
   	Eigen::Matrix4f r;
   	Eigen::Vector4f v, v1, v2, n;
	float s;

    if (din == 2) {
        //assert(markers.size() == 2);
        r << 1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1;
        r(0,0) = 0.5;
        r(0,1) = -1*std::sqrt(3)/2;
        r(1,0) = std::sqrt(3)/2;
        r(1,1) = 0.5;
        markers.push_back( r * (markers.at(1) - markers.at(0)) + markers.at(0) );
        redundant = true;
    } else if (din == 3) {
		//assert(markers.size() == 3);
		v = markers.at(0) + markers.at(1) + markers.at(2);
		v = v / 3;
		v1 = markers.at(1) - markers.at(0);
		v2 = markers.at(2) - markers.at(0);
		n = v1.cross3(v2);
		n.normalize();
		s = (v1.norm() + v2.norm() + (markers.at(2) - markers.at(1)).norm()) / 3;
		n = n * s;
		v = v + n;
		markers.push_back(v);
        redundant = true;
    }
}

/**
 *  remove imaginary marker
 */
void RbfBaseSolver::delRedundant(){
    markers.erase(markers.begin() + din);
    redundant = false;
}

/**
 *  add single marker to input dataset
 */
void RbfBaseSolver::addMarker(Eigen::Vector4f const & m) {
	markers.push_back(m);
}

/**
 *  add many makers to input dataset
 */
void RbfBaseSolver::addMarker(std::vector<Eigen::Vector4f> const & mvec) {
	markers.insert( markers.end(), mvec.begin(), mvec.end());
}

/**
 *  discover rbf inverse matrix for current dataset points
 */
void RbfBaseSolver::prepare() {
    // verify input data for valid Z entry
	assert(markers.size() > 0);
	//std::cout << "redundant = " << redundant << std::endl;
    if (redundant) delRedundant();
    for (unsigned int i=1; i<markers.size(); ++i)
        if (markers.at(0)(2) != markers.at(i)(2)) {
            din = 3;
            break;
        }
    if (markers.size() != (din + 1)) addRedundant();

    Eigen::MatrixXf C = Eigen::ArrayXXf::Zero(markers.size() + din + 1, markers.size() + din + 1); // zero matrix
    Eigen::VectorXf one = Eigen::ArrayXf::Constant(markers.size(), 1); // one vector
    Eigen::MatrixXf xyz = Eigen::ArrayXXf::Zero(markers.size(), din); // one vector

    // distance submatrix and xyz init
    for (unsigned int i=0; i < markers.size(); ++i) {
        xyz.row(i) = markers.at(i).head(din);
        for (unsigned int j=i+1; j < markers.size(); ++j)
            C(i,j) = C(j,i) = phi(markers.at(i), markers.at(j));
    }
    // markers submatrix
    C.block(0, xyz.rows(), one.size(), 1) = one;
    C.block(0, xyz.rows() + 1, xyz.rows(), xyz.cols()) = xyz;
    // markers submatrix
    C.block(xyz.rows(), 0, 1, one.size()) = one.transpose();
    C.block(xyz.rows() + 1, 0, xyz.cols(), xyz.rows()) = xyz.transpose();
    // invert
	Eigen::FullPivLU<Eigen::MatrixXf> lu(C);
    //assert(lu.isInvertible());
	inverse = lu.inverse();
	//inverse = C.inverse();
	//std::cout << "is invertible = " << lu.isInvertible() << std::endl;
	//std::cout << "C = " << std::endl << C << std::endl;
	//std::cout << "inverse = " << std::endl << inverse << std::endl;
}

/**
 * \param m array of known output values
 * \return a vector representing polynomial coefficients
 * 
 *  get polynomial coefficients for rbf given an image vector
 */
Eigen::VectorXf RbfBaseSolver::solve(Eigen::VectorXf const & m) {
    assert(inverse.cols() == m.size());
    //std::cout << "vector = " << std::endl << m << std::endl;
	return (inverse * m);
}

/**
 * \param p point
 * \return array of distance between p and recorded markers
 * 
 * get rbf distance vector for given point against all markers.
 */
Eigen::VectorXf RbfBaseSolver::getDistVec(Eigen::Vector4f const & p) {
    Eigen::VectorXf r = Eigen::ArrayXXf::Zero(markers.size() + din + 1, 1);

    for (unsigned int i=0; i<markers.size(); ++i) {
        r(i) = phi(markers.at(i), p);
    }
    r(markers.size()) = 1;
    for (unsigned int i=0; i < din; ++i) r(markers.size() + i + 1) = p(i);
	return r;
}

/**
 * \param sol array of RBF coefficients
 * \param p input value to be interpolated
 * \return interpolated value
 * 
 * get estimated result for input value, considering the given rbf 
 * polynomial coefficients
 */
float RbfBaseSolver::getValue(Eigen::VectorXf const & sol, Eigen::Vector4f const & p) {
	Eigen::VectorXf r = getDistVec(p);
	//std::cout << "dist vector = " << r.transpose() << std::endl;
	//std::cout << "dist vector adjoint = " << r.adjoint() << std::endl;
    assert(r.size() == sol.size());
    return (r.adjoint()*sol);
}

/**
 * dump variables for debugging purposes
 */    
void RbfBaseSolver::dump() {

	std::cout << "din = " << din << std::endl;
	std::cout << "redundant = " << redundant << std::endl;
	std::cout << "markers" << std::endl;
	for (unsigned int i=0; i < markers.size(); ++i)
		std::cout << i << " = " << markers.at(i).transpose() << std::endl;
	std::cout << "inverse" << std::endl << inverse << std::endl;
}

/**
 * add imaginary marker to pretend a simple interpolation function 
 * when there are few input points
 */
void RbfSimpleInterp::addRedundant() {
    RbfBaseSolver::addRedundant();
    if (din == 2) 
        image.push_back( (image.at(0) + image.at(1)) / 2 );
    else if (din == 3) 
        image.push_back( (image.at(0) + image.at(1) + image.at(2)) / 3 );
}

/**
 *  remove imaginary marker
 */
void RbfSimpleInterp::delRedundant() {
    RbfBaseSolver::delRedundant();
    image.erase(image.begin() + din);
}

/**
 *  add single image to input dataset
 */
void RbfSimpleInterp::addImage(float im) {
	image.push_back(im);
}

/**
 *  add imageset to input dataset
 */
void RbfSimpleInterp::addImage(std::vector<float> const & imvec) {
	image.insert( image.end(), imvec.begin(), imvec.end());
}

/**
 * solve rbf for current marker and image dataset storing 
 * rbf polynomial coefficients
 */
void RbfSimpleInterp::solve() {
    assert(markers.size() == image.size());
    for (unsigned int i=0; i<image.size(); ++i) {
        constval = (image.at(0) == image.at(i));
        if (!constval) break;
    }
    // just calculate if image is not constant
    if (!constval) {
        prepare();
        Eigen::VectorXf im = Eigen::ArrayXf::Zero(image.size() + din + 1);
        assert(inverse.cols() == im.size());
        for (unsigned int i=0; i<image.size(); ++i)
            im(i) = image.at(i);
		//std::cout << "din = " << din << std::endl;
		//std::cout << "im = " << im.size() << std::endl;
		//std::cout << "inverse cols = " << inverse.cols() << " rows = " << inverse.rows() << std::endl;
        rbfCoefs = RbfBaseSolver::solve(im);
    }
}

/**
 * \param m input data to be interpolated
 * \return interpolated value
 * 
 *  get estimated result for input data with saved rbf coefficients
 */
float RbfSimpleInterp::getImage(Eigen::Vector4f const & m) {
    if (constval)
        return image.at(0);
    else
        return getValue(rbfCoefs, m);
}

/**
 * dump variables for debugging purposes
 */    
void RbfSimpleInterp::dump() {

	RbfBaseSolver::dump();
	std::cout << "image" << std::endl;
	for (unsigned int i=0; i < image.size(); ++i)
		std::cout << i << " = " << image.at(i) << std::endl;
	std::cout << "constant = " << constval << std::endl;
	std::cout << "coefficients = " << rbfCoefs.transpose() << std::endl;
}

/**
 * \param rvec a rotation matrix vector representing a skeleton.
 * There should be one for each marker.
 * 
 * add skeleton to input dataset
 */
void RbfSkelInterp::addSkel(std::vector<Eigen::Matrix4f> const & rvec) {
    assert((skel.size() == 0) || (skel.at(0).size() == rvec.size()));
    skel.push_back(rvec);
}

/**
 * add imaginary marker to pretend a simple interpolation function 
 * when there are few input points
 */
void RbfSkelInterp::addRedundant() {
    std::vector<Eigen::Matrix4f> bset;
    Eigen::Matrix4f nr = Eigen::Matrix4f::Identity();

    RbfBaseSolver::addRedundant();
    if (din == 2) {
		assert(skel.size() > 1);
        for (unsigned int b=0; b < skel.at(0).size(); ++b) {
            for (unsigned int i=0; i<4; ++i)
                for (unsigned int j=0; j<4; ++j)
                    nr(i,j) = (skel.at(0).at(b)(i,j) + skel.at(1).at(b)(i,j))/2;
			orthonormalize(nr);
            bset.push_back(nr);
        }
    } else if (din == 3) {
		assert(skel.size() > 2);
        for (unsigned int b=0; b < skel.at(0).size(); ++b) {
            for (unsigned int i=0; i<4; ++i)
                for (unsigned int j=0; j<4; ++j)
                    nr(i,j) = (skel.at(0).at(b)(i,j) + skel.at(1).at(b)(i,j) + skel.at(2).at(b)(i,j))/3; 
			orthonormalize(nr);
            bset.push_back(nr);
        }
	}
    if (bset.size() > 0) skel.push_back(bset);
}

/**
 *  remove imaginary marker
 */
void RbfSkelInterp::delRedundant() {
    RbfBaseSolver::delRedundant();
    skel.erase(skel.begin() + din);
}

/**
 * solve rbf for current marker and skeleton dataset storing all 
 * rbf polynomial coefficients for each matrix element
 */
void RbfSkelInterp::solve() {

    Eigen::VectorXf px, im;
	assert(skel.size() > 0);

	// initialize coefficients and const verifyer
    constel.resize(skel.at(0).size());
	rbfBoneCoefs.resize(skel.at(0).size());
	// bones
	for (unsigned int b=0; b < constel.size(); ++b) {
		constel.at(b).resize(16);
		rbfBoneCoefs.at(b).resize(16);
		// matrix elements
		for (unsigned int i=0; i < 4; ++i) {
			for (unsigned int j=0; j < 4; ++j) {
				// frame skeletons
				for (unsigned int s=0; s < skel.size(); ++s) {
                	constel.at(b).at(4*i + j) = (skel.at(0).at(b)(i,j) == skel.at(s).at(b)(i,j));
					if (!constel.at(b).at(4*i + j)) break;
				}
			}
		}
	}

	// solve rbf and get its coefficients
    prepare();
    im = Eigen::ArrayXf::Zero(skel.size() + din + 1);
    //assert(inverse.cols() == im.size());
    // for each bone
    for (unsigned int b=0; b < rbfBoneCoefs.size(); ++b)
        // for each element matrix from the first 3 lines
        for (unsigned int i=0; i < 4; ++i)
            for (unsigned int j=0; j < 4; ++j)
                // get rbf coeff for element i,j if not constant
				if (!constel.at(b).at(4*i + j)) {
	                for (unsigned int s=0; s < skel.size(); ++s)
    	                im(s) = skel.at(s).at(b)(i,j);
        	        rbfBoneCoefs.at(b).at(4*i + j) = RbfBaseSolver::solve(im);
				}
}

/**
 * \param m the controller position among marker space
 * \return a interpolated new vector of rotation matrices
 * 
 *  produces new skeleton bones for a given controller position
 */
std::vector<Eigen::Matrix4f> const RbfSkelInterp::getNewSkel(Eigen::Vector4f const & m) {
    std::vector<Eigen::Matrix4f> bones;
    Eigen::VectorXf r = getDistVec(m);

	bones.resize(skel.at(0).size());
	for (unsigned int b=0; b < bones.size(); ++b) {
		bones.at(b) = Eigen::Matrix4f::Identity();
		for (unsigned int i=0; i < 4; ++i)
			for (unsigned int j=0; j < 4; ++j) 
				if (constel.at(b).at(4*i + j)) 
					bones.at(b)(i,j) = skel.at(0).at(b)(i,j);
				else
					bones.at(b)(i,j) = r.adjoint() * rbfBoneCoefs.at(b).at(4*i + j);
		orthonormalize(bones.at(b));
	}
	return bones;
}

/**
 * dump variables for debugging purposes
 */
void RbfSkelInterp::dump() {

	RbfBaseSolver::dump();
	for (unsigned int b=0; b < skel.at(0).size(); ++b)  {
		std::cout << "bone " << b << std::endl;
		for (unsigned int i=0; i < 4; ++i)
			for (unsigned int j=0; j < 4; ++j) {
				std::cout << "(" << i << ", " << j << ") = " ;
				for (unsigned int s=0; s < skel.size(); ++s) 
					std::cout << skel.at(s).at(b)(i,j) << " ";
				std::cout << std::endl << "constant = " << constel.at(b).at(4*i + j) << std::endl;
				std::cout << "coefficients = " << rbfBoneCoefs.at(b).at(4*i + j).transpose() << std::endl;
		}
	}
}

/**
 * \param n matrix to be orthonormalized
 * 
 * build a suitable rotation matrix.
 */
void RbfSkelInterp::orthonormalize(Eigen::Matrix4f & n) {
    Eigen::Vector4f u, v, w;
    float r;

    for (unsigned int i=0; i<10; ++i) {
        // normalize matrix
        n.col(0).normalize(); // x
        n.col(1).normalize(); // y
        n.col(2).normalize(); // z
        r = std::pow(n.col(0).dot(n.col(1)), 2) +
            std::pow(n.col(1).dot(n.col(2)), 2) +
            std::pow(n.col(2).dot(n.col(0)), 2);
        if (r < PZR) return;
        // find u, v, w
        u = n.col(1).cross3(n.col(2));
        v = n.col(2).cross3(n.col(0));
        w = n.col(0).cross3(n.col(1));
        n.col(0) = (n.col(0) + u)/2;
        n.col(1) = (n.col(1) + v)/2;
        n.col(2) = (n.col(2) + w)/2;
    }
    n.col(0).normalize();
    n.col(1).normalize();
    n.col(2).normalize();
}

