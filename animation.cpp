#include <animation.hpp>
#include <GL/gl.h>
#include <iostream>

/**
 * renders last skeleton move, markers and controllers
 */
void Animation::display() {
    skel.draw(GL_RENDER);
    drawMarkers(GL_RENDER);
}

/**
 * \return animation current skeleton
 * 
 * get current skeleton
 */    
Skeleton & Animation::getSkeleton() {
    return skel;
}

/**
 * \return array of recorded poses
 * 
 * get marked frame poses
 */
std::vector<Skeleton> const & Animation::getFrameSet() {
    return frameset;
}

/**
 * \return array with all markers positions
 * 
 * get pose frame markers
 */
std::vector<Eigen::Vector4f> & Animation::getMarkers() {
    return markers;
}

/**
 * \param x X window coordinate
 * \param y Y window coordinate
 * 
 * add a new frame. Saves current skeleton and current mouse position
 * as the marker position. Controller position is updated to the last
 * inserted marker. 
 */
void Animation::addNewFrame(float x, float y) {
	Eigen::Vector4f c;
	
	// add new marker and update controller position
	c(0) = x;
	c(1) = y;
	c(2) = 0;
	c(3) = 1;
	markers.push_back(c);
	controller.at(contIdx).scoord = c;
	controller.at(contIdx).mref.push_back(markers.size());
	// record current skeleton
	skel.sJoint = skel.joints.size();
	skel.sBone = skel.bones.size();
	sMarker = markers.size() - 1;
	sContr = contIdx;
	frameset.push_back(skel);
	glGetDoublev(GL_PROJECTION_MATRIX, pm);
}

/**
 * \param mode opengl mode: rendering or selection
 * 
 * draw keyframe points and controller
 */
 void Animation::drawMarkers(GLenum mode) {
    double mv[16];
    int vp[4];
	double nx, ny, nz;
	float *ptr;
	unsigned int off = skel.joints.size() + skel.bones.size();
	
	// draw markers
	glColor3f(0,1,1);
	glGetDoublev(GL_MODELVIEW_MATRIX, mv);
	glGetIntegerv(GL_VIEWPORT, vp);	
    for (unsigned int i=0; i < markers.size(); ++i) {
		ptr = markers.at(i).data();
		gluUnProject(ptr[0]*vp[2] , vp[3]*(1 - ptr[1]), ptr[2], mv, pm, vp, &nx, &ny, &nz);
		//std::cout << "frame point (" << nx << ", " << ny << ", " << nz <<  ")" << std::endl;
		if (mode == GL_SELECT) glLoadName(i + off);
		glPushMatrix();
		glTranslatef(nx, ny, nz);
		drawSphere(0.2, 30, 30);
		glPopMatrix();
    }
    // draw controllers
    if (markers.size() > 0) {
		glColor3f(1,0,1);
		for (unsigned int i=0; i < controller.size(); ++i) {
			ptr = controller.at(i).scoord.data();
			gluUnProject(ptr[0]*vp[2] , vp[3]*(1 - ptr[1]), ptr[2], mv, pm, vp, &nx, &ny, &nz);
			if (mode == GL_SELECT) glLoadName(markers.size() + off + i);
			glPushMatrix();
			glTranslatef(nx, ny, nz);
			drawSphere(0.3, 30, 30);
			glPopMatrix();
		}
	}
}

/**
 *  move controller and update current skeleton
 */
void Animation::animate() {
	std::vector<Eigen::Matrix4f> bns;
	
	if (sContr != controller.size()) {
		//std::cout << "interpolating new skeleton" << std::endl;
		bns = rbfint.getNewSkel(controller.at(sContr).scoord);
		assert(bns.size() == (skel.bones.size() + 1));
		skel.joints.at(0).setMatrix(bns.at(0));
		for (unsigned int i=0; i < skel.bones.size(); ++i)
			skel.bones.at(i).setMatrix(bns.at(i+1));
		skel.updateBonesRef();
	}	
}

/**
 *  prepare data for animation
 */
void Animation::setContext() {
	Skeleton s;
	std::vector<Eigen::Matrix4f> bns;
	
	if (frameset.size() < 2) {
		std::cout << "Not enough frame poses to interpolate." << std::endl;
		return;
	}
	assert(markers.size() == frameset.size());
	// add markers
	rbfint.reset();
	rbfint.addMarker(markers);
	// add skeletons
	for (unsigned int i=0; i < frameset.size(); ++i) {
		bns.clear();
		s = frameset.at(i);
		bns.resize(s.bones.size() + 1);
		bns.at(0) = s.joints.at(0).getMatrix();
		for (unsigned int i=0; i < s.bones.size(); ++i) 
			bns.at(i+1) = s.bones.at(i).getMatrix();
		rbfint.addSkel(bns);
	}
	rbfint.solve();
}

/**
 * add a new controller to the controller set
 * 
 * \note not yet implemented
 */
void Animation::addNewController(float x, float y) {
	// to do
}
/*
 *  function for debugging purposes
 */ 
void Animation::printMarkers() {
	std::cout << "controller = (" << controller.at(contIdx).scoord.transpose() << ")" << std::endl;
	std::cout << "total of markers = " << markers.size() << std::endl;
	for (unsigned int i=0; i < markers.size(); ++i)
		std::cout << "marker " << i << " = (" << markers.at(i).transpose() << ")" << std::endl;
}
